# Ausblick

Auf Basis dieser Darstellung des Protokolls und seiner Verwendung in OpenRefine sollen die folgenden Abschnitte einen Ausblick auf die Arbeiten der W3C Entity Reconciliation Community Group und das weitergehende Ökosystem rund um die Reconciliation-API geben.

## W3C Community Group

Aufgabe der W3C Community Group *Entity Reconciliation* ist die Entwicklung einer Web-API, mit der Datenanbieter einen Abgleich von Drittanbieterdaten mit den eigenen Identifikatoren ermöglichen können. Ausgangspunkt der Community Group bildet die beschriebene Implementierung in OpenRefine. Diese API soll zunächst dokumentiert und dann auf Basis der Erfahrungen mit der API zu einem Standard weiterentwickelt werden.[^1] Als ein Beispiel sei hier eine Diskussion zur Frage der Nachvollziehbarkeit von Algorithmen zur Bewertung (*Scoring*) von Reconciliation-Kandidaten genannt[^2], die anschließend Einzug in das Protokoll gefunden hat[^3].

Diese Entwicklung einer Web-API durch die Community Group umfasst innerhalb einer GitHub-Organisation[^4] die konkrete Arbeit an den eigentlichen Spezifikationenen, der eigenen Charter der Gruppe, sowie einer Webanwendung zum Testen von Reconciliation-Diensten und einer Erfassung des Reconciliation-Ökosystems von Diensten, Clients und sonstiger Software rund um die Reconciliation-API. Die Arbeit der Gruppe umfasst neben den etablierten Anwendungsfällen des Zusammenführens von Daten aus verschiedenen Quellen auch ähnliche Anwendungsfälle wie die Deduplizierung von Daten aus einer einzigen Quelle. Die Gruppe ist offen und freut sich über jegliche Art von Mitarbeit und Unterstützung.

[^1]: Die vollständige Charter der Gruppe findet sich unter https://reconciliation-api.github.io/charter/.
[^2]: https://lists.w3.org/Archives/Public/public-reconciliation/2020Jul/0000.html
[^3]: https://github.com/reconciliation-api/specs/pull/38
[^4]: https://github.com/reconciliation-api

## Reconciliation-Ökosystem

Die *Reconciliation service test bench*, eine Webanwendung zum Testen von Reconciliation-Diensten ist zugleich ein Werkzeug für die Entwicklung eines eigenen Reconciliation-Dienstes und in Form einer zentralen Instanz[^5] eine Übersicht der in Wikidata verzeichneten[^6] Reconciliation-Dienste mit ihren jeweils unterstützen Features.

Über diese Übersicht der Dienste hinaus, gibt es im Rahmen der Community Group eine Erfassung des Reconciliation-Ökosystems von Diensten, Clients und sonstiger Software rund um die Reconciliation-API[^7]. Hier findet sich etwa eine Übersicht alternativer Clients, die statt OpenRefine mit einem Reconciliation-Dienst kommunizieren, z.B. das Mapping-Tool *Cocoda* [@balakrishnan_dfg-projekt_2016] oder die *Alma Refine* App[^8] mit GND-Integration[^9].

Im Reconciliation-Ökosystem finden sich also reichlich Dokumentation, Werkzeuge und Beispiele für die Entwicklung eigener Reconciliation-Dienste. Über einen solchen Dienst kann jeder Datenanbieter, der eigene Identifikatoren prägt, seine Daten über eine einheitliche API zur Integration durch Dritte zur Verfügung stellen. So wird, auch über den Bereich der Inhaltserschließung hinaus, durch das beschriebene Protokoll eine einheitliche Erfassung von Entitäten ermöglicht, ohne dass diese in allen vorliegenden Datenquellen einheitlich identifiziert sein müssen.

[^5]: https://reconciliation-api.github.io/testbench/
[^6]: https://reconciliation-api.github.io/census/services/#how-to-add-a-service-to-the-test-bench
[^7]: https://reconciliation-api.github.io/census/
[^8]: https://developers.exlibrisgroup.com/blog/how-to-install-and-use-the-alma-refine-cloud-app-2-2/
[^9]: https://developers.exlibrisgroup.com/blog/how-to-use-the-alma-refine-cloud-app-for-service-gnd/