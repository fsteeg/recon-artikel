# Ein Protokoll für den Datenabgleich im Web am Beispiel von OpenRefine und der GND

Fabian Steeg & Adrian Pohl, Hochschulbibliothekszentrum des Landes NRW

Beitrag im Sammelband zur Qualität der Inhaltserschließung, Preprint 2020-11-13

## Überblick

 Normdaten spielen als externe Datenquellen eine wichtige Rolle für die Qualität der Inhaltserschließung. OpenRefine ermöglicht über eine Reconciliation-API den Abgleich eigener Daten mit externen Datenquellen. Das hbz stellt mit lobid-gnd eine solche Datenquelle für die GND bereit. Dieser Beitrag stellt nach einer Einordnung der Reconciliation für die Inhaltserschließung die Möglichkeiten der Reconciliation-API anhand ihrer Verwendung in OpenRefine zum Abgleich mit der GND dar. Anschließend wird als Ausblick die Arbeit der Entity Reconciliation Community Group des W3C vorgestellt, in der ein Protokoll zur Standardisierung dieser Funktionalität entwickelt wird.
 
 Der Beitrag behandelt so die Qualität des Datenaustauschs (speziell die Beschreibung und Verbesserung des Protokolls selbst im Rahmen einer Standardisierung), die Qualität von Datensätzen im Kontext anderer Datensätze (speziell die Einheitlichkeit der Verknüpfung und Ermöglichung von Datenanreicherung durch Nutzung von Normdaten statt der Verwendung von Freitext), sowie die Qualität von Werkzeugen und Algorithmen (speziell von Werkzeugen, die das Protokoll nutzen, konkret OpenRefine, sowie durch das Protokoll ermöglichte Arbeitsabläufe).