# Protokoll

Der folgende Abschnitt beschreibt die einzelnen Elemente des Protokolls für den Datenabgleich im Web und ihre Verwendung am Beispiel von OpenRefine und der GND. Das Protokoll hat seinen Ursprung in seiner Implementierung in OpenRefine. Ausgehend davon soll es im Rahmen des W3C standardisiert werden [@delpeuch2020draft]. Dies hat gegenüber dem umgekehrten Ansatz (zuerst wird ein Protokoll standardisiert, dann wird es implementiert) den Vorteil, dass das Protokoll erwiesenermaßen praxistauglich ist. Es entspricht in diesem Aspekt auch der in der IETF entstandenen pragmatischen Grundhaltung der Internet-Standardisierung [@alvestrand2009development]. Die folgende Beschreibung des Protokolls kann anhand der Implementierung in lobid-gnd [@steeg2019lobidgnd] praktisch nachvollzogen werden.[^1]

[^1]: Eine Dokumentation der API von lobid-gnd findet sich unter https://lobid.org/gnd/api

## JSON

Das im Folgenden beschriebene Protokoll basiert auf JSON, dem bereits ab 2005 sehr populärem und spätestens nach seiner Standardisierung 2013 weit etabliertem Format für Web-basierten Datenaustausch [@target2017json]. Grundelement von JSON ist eine Attribut-Wert Zuordnung, z.B.:

```
{
  "Attribut_1": "Wert_1",
  "Attribut_2": "Wert_2"
}
```

Die Metadaten, Anfragen und Antworten dieses Protokolls werden mit JSON formuliert.

## Service

Ein Reconciliation Service beschreibt sich selbst in einem Service Manifest. Dieses JSON-Dokument definiert mindestens einen Namen in `name`, ein Präfix zur Identifikation der gelieferten Entitäten (z.B. zur Identifikation einer GND-Nummer wie `118624822` als `https://d-nb.info/gnd/118624822`) in `identifierSpace` und den Typ der gelieferten Entitäten (und damit die Ontologie samt verfügbaren Properties für die Entitäten) in `schemaSpace`:

```
{
  "name": "GND reconciliation for OpenRefine",
  "identifierSpace": "https://d-nb.info/gnd/",
  "schemaSpace": "https://d-nb.info/standards/elementset/gnd#AuthorityResource"
}
```

## Reconciliation Queries

Ein Dienst mit einem solchen Service Manifest kann in einem Reconciliation Client eingebunden werden und steht dann für Reconciliation Queries zur Verfügung (s. Abb. 1). Das Service Manifest kann weitere, optionale Hilfsdienste und Unterstützung für Data Extension deklarieren (s. unten).

![Abb1_Steeg.png](../img/Abb1_Steeg.png)

Abb. 1: Der Reconciliation-Dialog mit zahlreichen Konfigurationsmöglichkeiten in OpenRefine

### Einfache Anfragen

In der einfachsten Form werden nur Namen an den Dienst geschickt. Dies erfolgt für alle abzugleichenden Werte in einer einzigen Anfrage. Auf Ebene der OpenRefine-Oberfläche bedeutet dies die Auswahl der entsprechenden Spalte (z.B. für 'Name' in Abb. 1).

Auf Ebene des Protokolls handelt es sich um ein JSON-Objekt, bei dem jeder Wert durch einen eindeutigen Schlüssel (hier `q1` und `q2`) identifiziert wird. Dazu müssen wir uns ansehen, wie mit JSON geschachtelte Struktur ausgedrückt werden kann: der Wert eines Attributs muss selbst nicht eine Zeichenkette sein (wie `"Wert_1"` im ersten JSON-Beispiel oben), sondern kann selbst wieder JSON sein, z.B. `{ "query": "Hans-Eberhard Urbaniak" }`.

Für eine Spalte bzw. Anfrage mit zwei Werten stellt sich eine minimale Anfrage dann so dar:

```
{
  "q1": { "query": "Hans-Eberhard Urbaniak" },
  "q2": { "query": "Ernst Schwanhold" }
}
```

Eine solche minimale Reconciliation Query lässt sich etwa mit folgender URL im Browser durchführen:

`https://lobid.org/gnd/reconcile/?queries={"q1":{"query":"Twain, Mark"}}`

Die im Browser ausgelieferte Antwort ist ein JSON-Dokument. Zur komfortableren Anzeige im Browser, etwa mit Syntax-Coloring und einklappbaren Unterabschnitten, existieren diverse JSON-Browser-Plugins. Auf der Kommandozeile können die Daten etwa mit dem vielseitigen Werkzeug `jq` verarbeitet werden:[^2]

`curl --data 'queries={"q1":{"query":"Twain, Mark"}}' https://lobid.org/gnd/reconcile/ | jq`

[^2]: Für Details und weitere Beispiele s. https://shapeshed.com/jq-json/

Die Antwort besteht aus einer Reihe von Vorschlägen (in JSON als Array innerhalb von `[` und `]` ausgedrückt) für jedes Element der Anfrage (hier gekürzt: nur `q1` und die ersten zwei Vorschläge):

```
{
  "q1": {
    "result": [
      {
        "id": "118624822",
        "name": "Twain, Mark",
        "score": 84.15378,
        "match": true,
        "type": [{"id": "DifferentiatedPerson", "name": "Individualisierte Person"}]
      },
      {
        "id": "1045623490",
        "name": "Bezirkszentralbibliothek Mark Twain. Schreibwerkstatt",
        "score": 78.29902,
        "match": false,
        "type": [{"id": "CorporateBody", "name": "Körperschaft"}]
      }
    ]
  }
}
```

Wir sehen hier als ersten Vorschlag also Mark Twain selbst (`118624822`, Typ `Individualisierte Person`), sowie als zweiten Vorschlag eine `Körperschaft` im dem Identifikator `1045623490`. Die Eindeutigkeit der Identifikatoren ergibt sich durch den im Service Manifest angegebenen `identifierSpace` (s. Abschnitt *Service*). Der gemeinsame Namensraum `https://d-nb.info/gnd/` für GND-Nummern ermöglicht so die Interoperabilität verschiedener Dienste auf Basis der GND.

Neben Identifikator und Typ enthalten die Vorschläge die Felder `name`, `score` und `match` als Details zum jeweiligen Vorschlag. Score ist ein Maß des Dienstes für die Übereinstimmung des Vorschlages mit der Anfrage (d.h. je höher der Score, desto besser der Vorschlag) und `match` drückt per Wahrheitswert aus, ob der Vorschlag nach internen Kriterien des Dienstes als Treffer zu dem Vorschlag bewertet wird.

Diese Vorschläge können dem Nutzenden im Reconciliation Client angezeigt werden (s. Abb. 2, pro Name sehen wir die entsprechenden Vorschläge, bzw. den Namen in Fett bei der automatisch als Treffer gewerteten Entität).

### Weitere Metadaten

Dadurch, dass mit jedem Element der Anfrage (z.B. `q1` oben) wieder JSON assoziert ist, können jedem Element der Anfrage zusätzlich zum Namen weitere Metadaten hinzugefügt werden, etwa der gesuchte Entitätstyp in `type` oder eine Begrenzung der vom Dienst gelieferten Vorschläge in `limit`:

```
{
  "q0": {
    "query": "Christel Hanewinckel",
    "type": "DifferentiatedPerson",
    "limit": 5
  },
  "q1": {
    "query": "Franz Thönnes",
    "type": "DifferentiatedPerson",
    "limit": 5
  }
}
```

Diese können in einem Reconciliation Client bei der Nutzung konfiguriert werden (s. Abb. 1, Typauswahl oben links: *Reconcile each cell to an entity of one of these types*, Beschränkung unten links: *Maximum number of candidates to return*).

Für eine höhere Transparenz der oben beschriebenen Bewertung in der Antwort (`score`, `match`) kann ein Reconciliation Service für jedes `result` auch spezifische `features` zurückgegeben, die eine differenziertere Bewertung der Vorschläge durch den Reconciliation Client ermöglichen, z.B. eine separate Bewertung der Übereinstimmung des Namens (in `name_tfidf`) bzw. des angeforderten Typs (in `type_match`):

```
"features": [
  {
    "id": "name_tfidf",
    "value": 334.188
  },
  {
    "id": "type_match",
    "value": 13.78
  }
]
```

Auf dieser Basis könnte ein Reconciliation Client neben der Auswertung des `match` Wertes (s. Abb. 1, links unten: *Auto-match candidates with high confidence*) selbst entscheiden, ob etwa eine Übereinstimmung beim angeforderten `type` eine geringere Übereinstimmung des Names ausgleicht und doch als Treffer zu werten ist.

### Zusätzliche Properties

Neben dem abzugleichenden Namen und den oben beschriebenen Metadaten können weitere Daten mitgeschickt werden, um die Qualität des Abgleichs zu erhöhen, d.h. um mit höherer Wahrscheinlichkeit den korrekten Identifikator vom Dienst angeboten zu bekommen. Dies können bei Personen etwa Lebensdaten oder Berufe sein. Auf Ebene der OpenRefine-Oberfläche sind diese weiteren Daten zusätzliche Spalten der Tabelle (z.B. Spalten Beruf, Geburtsjahr, Sterbejahr; s. Abb. 1, oben rechts: *Also use relevant details from other columns*). Auf Ebene des Protokolls werden diese als `properties` abgebildet:[^1]

```
"properties": [
  {
    "pid": "professionOrOccupation",
    "v": "Politik*"
  },
  {
    "pid": "affiliation",
    "v": "http://d-nb.info/gnd/2022139-3"
  }
]
```

[^1]: Details zu diesem Beispiel finden sich unter https://blog.lobid.org/2019/09/30/openrefine-examples.html#occupations-and-affiliations

![Abb2_Steeg.png](../img/Abb2_Steeg.png)

Abb. 2: Ergebnisse der Reconciliation mit Vorschlägen und Vorschau zur Auswahl von Kandidaten

## Hilfsdienste

Neben der zentralen Funktionalität der Reconciliation Queries kann ein Reconciliation Service weitere Dienste anbieten. Dies sind zum einen Hilfsdienste, die die Kernfunktionalität erweitern, insbesondere in Form von Vorschauen und Vorschlägen, sowie zum anderen Dienste zur Verwendung der abgeglichenen Entitäten zur Datenanreicherung der lokalen Datensätze (Data Extension).

### Anzeige

Im Wesentlichen gibt es zwei Dienste zur Anzeige von Entitäten. Zum einen kann der Reconciliation Service in seinem Service Manifest deklarieren, wo Entitäten auf Basis eines Identifikators angezeigt werden können. Dies erfolgt über einen Eintrag `view` mit einer URL, die einen Platzhalter für den Identifikator enthält, z.B.:

```
"view": {
  "url": "https://lobid.org/gnd/{{id}}"
}
```

Ein Reconciliation Client kann damit einen Link zu einer Entität erzeugen, indem in `"https://lobid.org/gnd/{{id}}"` die Zeichenkette `{{id}}` durch den eigentlichen Identifikator ersetzt wird, um z.B. oben den ersten Vorschlag zu `Mark Twain` mit einem Link zu `https://lobid.org/gnd/118624822` zu hinterlegen (s. Abb. 2, Vorschläge sind mit Links hinterlegt).

Für eine engere Integration in einen Reconciliation Client gibt es darüber hinaus die Möglichkeit, eine Vorschau zu liefern. Der Reconcilition Service definiert dazu in vergleichbarer Form einen Service in seinem Service Manifest:

```
"preview": {
  "height": 100,
  "width": 320,
  "url": "https://lobid.org/gnd/{{id}}.preview"
}
```

Im Unterschied zum `view` wird hier eine Größe definiert, so dass der Client einen entsprechend großen Vorschau-Bereich erzeugen kann. Hier liefert der Dienst wie bei `view` HTML, das direkt angezeigt werden kann, allerdings muss dafür bei `preview` keine neue Seite verlinkt werden, sondern die Vorschau kann etwa in einem Popup angezeigt werden (s. Abb. 2, Vorschau für Kandidaten mit Bild, Lebensdaten, Beruf und Typ).

### Vorschläge

Die *Suggest* Hilfsdienste dienen zur Anzeige von Vorschlägen an verschiedenen Stellen des Datenabgleichs in einem Reconciliation Client. Vorgeschlagen werden können Entitäten, Properties und Typen. Dazu wird jeweils im Service Manifest der eigentliche Vorschlagsdienst, sowie analog zum `preview` oben, ein sogenannter Flyout-Dienst für kleine, integrierte Darstellungen:

```
"suggest": {
    "property": {
      "service_url": "https://lobid.org/gnd/reconcile",
      "service_path": "/suggest/property",
      "flyout_service_path": "/flyout/property?id=${id}"
    },
    "entity": {
      "service_url": "https://lobid.org/gnd/reconcile",
      "service_path": "/suggest/entity",
      "flyout_service_path": "/flyout/entity?id=${id}"
    },
    "type": {
      "service_url": "https://lobid.org/gnd/reconcile",
      "service_path": "/suggest/type",
      "flyout_service_path": "/flyout/type?id=${id}"
    }
}
```

Alle Vorschlagsdienste erwarten einen Query-Parameter `prefix`, in dem die bisher vom Nutzenden eingegebene Zeichenkette übergeben wird. Dies kann im Client etwa genutzt werden, um vorzuschlagen, mit welchem GND-Property mitgeschickte Daten assoziiert werden (s. oben, Zusätzliche Properties). Wird im Client etwa an der entsprechenden Stelle `beruf` eingegeben, wird intern folgende Anfrage an den `property` Hilfsdienst gesendet:

`https://lobid.org/gnd/reconcile/suggest/property?prefix=beruf`

Die Antwort zu dieser Anfrage lautet (kann wie oben im Browser oder per curl nachvollzogen werden):

```
{
  "code": "/api/status/ok",
  "status": "200 OK",
  "prefix": "beruf",
  "result": [
    {
      "id": "professionOrOccupation",
      "name": "Beruf oder Beschäftigung"
    },
    {
      "id": "professionOrOccupationAsLiteral",
      "name": "Beruf oder Beschäftigung (Literal)"
    },
    {
      "id": "professionalRelationship",
      "name": "Berufliche Beziehung"
    }
  ]
}

```

Die drei gelieferten Properties können dann dem Nutzenden zur Auswahl vorgeschlagen werden (s. Abb. 1, rechts oben: *Also use relevant details from other columns* sowie Abb. 3, links: *Add Property*). Analog kann vor dem Abgleich ein spezifischer Typ vorgeschlagen werden (`type` Suggest-Dienst, s. Abb. 1, unten links: *Reconcile against type*), oder nach dem Abgleich gezielt nach einem Treffer gesucht werden (`entity` Suggest-Dienst, s. Abb. 2, unterhalb der Vorschläge: *Search for match*).

## Data Extension

Das Data Extension Protokoll ermöglicht eine Datenareicherung auf Basis der abgeglichenen Treffer. Es besteht aus zwei wesentlichen Teilen: erstens der Kommunikation über die zur Datenanreicheung verfügbaren Properties (s. Abb. 3, linker Bereich) und zweitens der eingentlichen Anreicherung mit den Werten der ausgewählten Properties (s. Abb. 3, rechter Bereich).

![Abb3_Steeg.png](../img/Abb3_Steeg.png)

Abb. 3: Dialog zur Data Extension mit verfügbaren Properties und Vorschau für ergänzte Spalten

### Property-Vorschläge

Zunächst muss wieder das Service Manifest die Unterstützung für Property-Vorschläge deklarieren:

```
"propose_properties": {
  "service_url": "https://lobid.org",
  "service_path": "/gnd/reconcile/properties"
}
```

Ein solcher Dienst liefert die für einen bestimmten Typ (z.B. `Work`) verfügbaren Properties:

`GET https://lobid.org/gnd/reconcile/properties?type=Work`

Hier eine gekürzte Antwort:

```
{
  "type": "Work",
  "properties": [
    {
      "id": "abbreviatedNameForTheWork",
      "name": "Abgekürzter Name des Werks"
    },
    {
      "id": "firstAuthor",
      "name": "Erste Verfasserschaft"
    },
    {
      "id": "preferredName",
      "name": "Bevorzugter Name"
    },
    {
      "id": "relatedConferenceOrEvent",
      "name": "In Beziehung stehende Konferenz oder Veranstaltung"
    }
  ]
}
```

Aus diesen Properties können dann von Nutzenden diejenigen ausgewählt werden, die zur eigentlichen Datenanreicherung verwendet werden sollen (s. Abb. 3, links: *Suggested Properties*).

### Extension-Anfragen

In der einfachsten Form werden bei der eigentlichen Anfrage zur Data Extension die Identifikatoren der zu verwendenden Entitäten, sowie die gewünschten Properties geschickt, z.B.:

```
{
    "ids": [
        "1081942517",
        "4791358-7"
    ],
    "properties": [
        {"id": "preferredName"},
        {"id": "firstAuthor"}
    ]
}
```

Als vollständige Anfrage etwa:

```
GET https://lobid.org/gnd/reconcile/?extend=
{"ids":["1081942517","4791358-7"],"properties":[{"id":"preferredName"},{"id":"firstAuthor"}]}
```

Die Antwort liefert die entsprechenden Daten:

```
{
  "meta": [
    {"id": "preferredName", "name": "Bevorzugter Name"},
    {"id": "firstAuthor", "name": "Erste Verfasserschaft"}
  ],
  "rows": {
    "1081942517": {
      "preferredName": [{"str": "Autobiography of Mark Twain"}],
      "firstAuthor": [{"id": "118624822", "name": "Twain, Mark"}]
    },
    "4791358-7": {
      "preferredName": [{"str": "Die größere Hoffnung (1960)"}],
      "firstAuthor": [{"id": "118501232","name": "Aichinger, Ilse"}]
    }
  }
}
```

Wir sehen zunächst, unter `meta`, Informationen zu den angereicherten Daten: die Identifikatoren und Namen für die angereicherten Properties. Dies ist vergleichbar mit der Header-Zeile einer Tabelle (s. Abb. 3, rechts, fett gedruckte Kopfzeile). Im folgenden haben wir die einzelnen Datensätze in `rows`, jeweils unter dem Identifikator der Entität (hier das Werk), z.B. `1081942517` die jeweiligen Properties (hier `preferredName` und `firstAuthor`). Die Struktur der Werte unterscheidet sich, da die Ansetzungsformen aus `preferredName` einfache Zeichenketten sind, während in Autorenschaft (`firstAuthor`) GND-Entitäten enthalten sind, die jeweils wieder einen Identifikator und einen Namen haben. Diese Daten können dann im Reconciliation Client den lokalen Daten hinzugefügt werden (s. Abb. 3, rechts).

Neben dieser Definition von Properties allein über ihre Identifikatoren (z.B. oben `{"id":"preferredName"}`) besteht die Möglichkeit, Properties zu konfigurieren. So kann etwa die Zahl der Werte für ein bestimmtes Feld bei der Anreicherung eingeschränkt werden, wenn z.B. nicht alle, sondern nur die ersten 5 Namensvarianten zurückgegeben werden sollen. Die Unterstützung für eine solche Konfiguration deklariert der Dienst zunächst etwa so:

```
"property_settings": [
  {
    "name": "limit",
    "label": "Limit",
    "type": "number",
    "default": 0,
    "help_text": "Maximum number of values to return per row (0 for no limit)"
  }
]
```

Neben der freien Eingabe soll die Konfiguration oft auch mit festen Werten erfolgen. So können für bestimmte Felder in der GND etwa Identifikatoren oder Namen geliefert werden. Damit Nutzende dies je nach Bedarf im Reconciliation Client auswählen können, definiert der Dienst zusätzlich zu den Werten oben `choices`, z.B.:

```
"property_settings": [
  {
    "name": "content",
    "label": "Content",
    "type": "select",
    "default": "literal",
    "help_text": "Content type: ID or literal",
    "choices": [
      { "value": "id", "name": "ID" },
      { "value": "literal", "name": "Literal" }
    ]
  }
]
```

Die in `choices` beschriebenen Optionen können dann in einem Reconciliation Client bei der Auswahl und Konfiguration der zur Datenanreicheung zu verwendenden Properties angezeigt werden, z.B. in Form eines Auswahlmenüs für die oben im Service Manifest deklarierten Werte (s. Abb. 3, rechts, jeweils: *configure* öffnet einen Konfigurationsdialog).

Bei der entsprechenden Data Extension Anfrage wird die jeweilige Konfiguration dann mitgeschickt, z.B.:

```
{
  "ids": [
    "10662041X",
    "1064905412"
  ],
  "properties": [
    {
      "id": "variantName",
      "settings": {
        "limit": "5"
      }
    },
    {
      "id": "professionOrOccupation"
    },
    {
      "id": "geographicAreaCode",
      "settings": {
        "limit": "1",
        "content": "id"
      }
    }
  ]
}
```

Hier werden für zwei Entitäten jeweils 3 Properties angefordert: erstens `variantName` (konfiguriert mit einem `limit` von 5), zweitens `professionOrOccupation` (ohne Konfiguration) und schließlich drittens`geographicAreaCode` mit einem `limit` von 1 und als `content` den Identifikator (über die im Service Manifest deklarierten Option mit `value`: `id`). Hier reichern wir unsere Daten also mit maximal 5 Namesvarianten, allen verfügbaren Berufen und einem Ländercode an.
