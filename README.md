# recon-artikel

Zettlr-Projekt. HTML-Export publiziert unter [https://fsteeg.codeberg.page/recon-artikel/](https://fsteeg.codeberg.page/recon-artikel/).

DOCX-Export über Beispieldatei. Setup in Zettlr (Preferences -> Advanced -> Pandoc command):

`pandoc "$infile$" -f markdown $outflag$ $tpl$ --reference-doc=$indir$/../vorgaben/bipra_formatvorlagebsp.doc $toc$ $tocdepth$ $citeproc$ $standalone$ --latex-engine=xelatex --mathjax -o "$outfile$"`

Zettlr führt intern die einzelnen Dateien zusammen und macht dann auf das Ganze sowas (hier nur für 1 Kapitel):

`pandoc "doc/3-ausblick.md" -f markdown -t docx --reference-doc=vorgaben/bipra_formatvorlagebsp.doc --toc-depth=3 --filter pandoc-citeproc --bibliography "referenzen.bib" -s --latex-engine=xelatex --mathjax -o "doc/recon-artikel.docx"`

Ausgabedatei manuell überarbeitet:

- Entfernt: Header 'I.1 Grundfunktionen bei Datenbankrecherchen'
- 3 mal: Abbildung entfernen, 'Hier AbbX_Steeg.png einfügen' ergänzen
- Literatur: jeweils Einrückung, X entfernen, URL-Abrufdaten ergänzen